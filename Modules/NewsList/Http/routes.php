<?php

Route::group(['middleware' => 'web', 'prefix' => 'newslist', 'namespace' => 'Modules\NewsList\Http\Controllers'], function()
{
    Route::get('/', 'NewsListController@index');
});
