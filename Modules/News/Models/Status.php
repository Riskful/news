<?php

namespace Modules\News\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = [
    	'title'
    ];
}
