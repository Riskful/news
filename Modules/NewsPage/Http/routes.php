<?php

Route::group(['middleware' => 'web', 'prefix' => 'newspage', 'namespace' => 'Modules\NewsPage\Http\Controllers'], function()
{
    Route::get('/', 'NewsPageController@index');
});
