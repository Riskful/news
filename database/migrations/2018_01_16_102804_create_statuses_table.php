<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->timestamps();
        });

        $data = $this->getData();
        foreach ($data as $d) {
            $status = new \Modules\News\Models\Status(['title' => $d]);
            $status->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statuses');
    }

    public function getData()
    {
        return array(
            0 => 'опубликовано',
            1 => 'черновик',
            2 => 'будет опубликовано',
            3 => 'в корзине'
        );
    }
}
